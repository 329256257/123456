package com.icss.yrbao.dao;

import com.icss.yrbao.bean.Inverst;

import java.util.ArrayList;
import java.util.List;

public interface InverstMapper {
    int deleteByPrimaryKey(Integer investid);

    int insert(Inverst record);

    Inverst selectByPrimaryKey(Integer investid);

    List<Inverst> selectAll();

    int updateByPrimaryKey(Inverst record);

	ArrayList<Inverst> findInverstByid(Integer loanid);

	boolean addInverat(Integer userid, Integer loanid, Integer number, String num);
}