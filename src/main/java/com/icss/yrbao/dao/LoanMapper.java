package com.icss.yrbao.dao;

import com.icss.yrbao.bean.Inverst;
import com.icss.yrbao.bean.Loan;

import java.util.ArrayList;
import java.util.List;

public interface LoanMapper {
    int deleteByPrimaryKey(Integer loanid);

    int insert(Loan record);

    Loan selectByPrimaryKey(Integer loanid);

    List<Loan> selectAll();

    int updateByPrimaryKey(Loan record);

	ArrayList<Loan> findAllLoan();

	Loan findLoanByid(Integer loanid);

	ArrayList<Inverst> findInverstByid(Integer loanid);

	boolean updateCountMoney(Integer loanid, Integer number);
}