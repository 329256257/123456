package com.icss.yrbao.dao;

import com.icss.yrbao.bean.Transaction;
import java.util.List;

public interface TransactionMapper {
    int deleteByPrimaryKey(Integer recordid);

    int insert(Transaction record);

    Transaction selectByPrimaryKey(Integer recordid);

    List<Transaction> selectAll();

    int updateByPrimaryKey(Transaction record);

	boolean insertTransaction(Integer userid, Integer number, String num);
}