package com.icss.yrbao.dao;

import com.icss.yrbao.bean.User;
import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userid);

    int insert(User record);

    User selectByPrimaryKey(Integer userid);

    List<User> selectAll();

    int updateByPrimaryKey(User record);

	User selectUser(User user);

	 void insertUser(User user);

	User loginSelect(String username, String password);

	User selectByid(Integer userid);

	void updateLogintime(String username, String password);

	boolean updateBalance(Integer userid, Integer number);
}