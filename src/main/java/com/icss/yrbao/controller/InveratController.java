package com.icss.yrbao.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.icss.yrbao.service.IInverstServicer;

@Controller
public class InveratController {
     @Resource(name = "inverstServicer")
	private IInverstServicer inverstServicer;

	public IInverstServicer getInverstServicer() {
		return inverstServicer;
	}

	public void setInverstServicer(IInverstServicer inverstServicer) {
		this.inverstServicer = inverstServicer;
	}
    
	@RequestMapping("toInverat")
	public void toInverat(Integer userid,Integer loanid,Integer number,HttpServletResponse response) throws IOException {
		boolean reslut=inverstServicer.toInverat(userid,loanid,number);
		response.getWriter().print(reslut);
	}
}
