package com.icss.yrbao.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.icss.yrbao.bean.User;
import com.icss.yrbao.service.IUserServicer;
import com.sun.javafx.sg.prism.NGShape.Mode;
import com.sun.org.apache.xpath.internal.operations.Mod;
@Controller
public class UserController {
	@Resource(name="userservice")
	private IUserServicer userservice;

	public IUserServicer getUserservice() {
		return userservice;
	}

	public void setUserservice(IUserServicer userservice) {
		this.userservice = userservice;
	}
	@RequestMapping("toLogin")
	public ModelAndView toLogin() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("login");
		return mv;
		
	}
	
	@RequestMapping("toRegister")
	public ModelAndView toRegister() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("registerA");
		return mv;
	}
	
	@RequestMapping("toRegisterB")
	public ModelAndView toRegisterB(User user) {
		ModelAndView mv=new ModelAndView();
	     User result = null;
	    if(!user.getUsername().equals("")&&user.getUsername()!=null&&user.getPassword()!=null && !user.getPassword().equals("")&&user.getPhone()!=null&&!user.getPhone().equals("")) {
	      result= userservice.regist(user);
	    	}
		   
		   if(result==null) {
			mv.setViewName("registerA");
			mv.addObject("msg","信息不完善或用户名已存在");
			System.out.println(404);
		   }else {
			mv.addObject("user",result);
			mv.setViewName("registerB");
		   }
		   System.out.println(result);
		
		return mv;
	}
	
	@RequestMapping("login")
	public ModelAndView login(String j_username,String password,HttpSession session) {
		ModelAndView mv=new ModelAndView();
		User u=userservice.login(j_username,password);
		session.setAttribute("user", u);
		
//		mv.addObject("user",u);
		mv.setViewName("personalCenter");
		return mv;
		
	}
	
     @RequestMapping("toMyCenter")
     public ModelAndView toMylCenter(Integer userid,HttpSession session) {
    	 ModelAndView mv=new ModelAndView();
 		User u=userservice.selectByid(userid);
 		session.setAttribute("user", u);
// 		mv.addObject("user",u);
 		mv.setViewName("personalCenter");
		return mv;
    	 
     }
     
    
     

}
