package com.icss.yrbao.controller;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.icss.yrbao.bean.Inverst;
import com.icss.yrbao.bean.Loan;
import com.icss.yrbao.service.IInverstServicer;
import com.icss.yrbao.service.ILoanServicer;

@Controller
public class LoanController {
	@Resource(name = "loanservicer")
	private ILoanServicer loanservicer;
	@Resource(name = "inverstServicer")
	private IInverstServicer inverstServicer;

	public ILoanServicer getLoanservicer() {
		return loanservicer;
	}

	public void setLoanservicer(ILoanServicer loanservicer) {
		this.loanservicer = loanservicer;
	}

	public IInverstServicer getInverstServicer() {
		return inverstServicer;
	}

	public void setInverstServicer(IInverstServicer inverstServicer) {
		this.inverstServicer = inverstServicer;
	}

	@RequestMapping("toList")
	public ModelAndView toList() {
		ModelAndView mv = new ModelAndView();
		ArrayList<Loan> loans = loanservicer.findAllLoan();
		mv.addObject("loans", loans);
		for (Loan loan : loans) {
			System.out.println(loan);
		}
		mv.setViewName("list");
		return mv;
	}

	@RequestMapping("toInfor")
	public ModelAndView toInfor(Integer loanid) {
		ModelAndView mv = new ModelAndView();
		 Loan ln=loanservicer.findLoanByid(loanid);
	
		  ArrayList<Inverst> inversts=inverstServicer.findInverstByid(loanid);
		  mv.addObject("inversts",inversts);
	
		  mv.addObject("loan",ln);
		
		mv.setViewName("infor");
		return mv;

	}


	@RequestMapping("toMain")
	public ModelAndView toMain() {
		ModelAndView mv = new ModelAndView();
		ArrayList<Loan> loans = loanservicer.findAllLoan();
		mv.addObject("loans", loans);
		for (Loan loan : loans) {
			System.out.println(loan);
		}
		mv.setViewName("main");
		return mv;

	}

}
