package com.icss.yrbao.bean;

import java.math.BigDecimal;
import java.util.Date;

public class Loan {
    private Integer loanid;

    private String loanName;

    private BigDecimal loanMoney;

    private String createdate;

    private BigDecimal loanRate;

    private String loanNumber;

    private Integer typeid;

    private Integer loanStatus;

    private BigDecimal countMoney;

    private Integer loanTime;

    private Integer userid;

    private String ordernumber;

    public Integer getLoanid() {
        return loanid;
    }

    public void setLoanid(Integer loanid) {
        this.loanid = loanid;
    }

    public String getLoanName() {
        return loanName;
    }

    public void setLoanName(String loanName) {
        this.loanName = loanName == null ? null : loanName.trim();
    }

    public BigDecimal getLoanMoney() {
        return loanMoney;
    }

    public void setLoanMoney(BigDecimal loanMoney) {
        this.loanMoney = loanMoney;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(BigDecimal loanRate) {
        this.loanRate = loanRate;
    }

    public String getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(String loanNumber) {
        this.loanNumber = loanNumber == null ? null : loanNumber.trim();
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(Integer loanStatus) {
        this.loanStatus = loanStatus;
    }

    public BigDecimal getCountMoney() {
        return countMoney;
    }

    public void setCountMoney(BigDecimal countMoney) {
        this.countMoney = countMoney;
    }

    public Integer getLoanTime() {
        return loanTime;
    }

    public void setLoanTime(Integer loanTime) {
        this.loanTime = loanTime;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber == null ? null : ordernumber.trim();
    }

	@Override
	public String toString() {
		return "Loan [loanid=" + loanid + ", loanName=" + loanName + ", loanMoney=" + loanMoney + ", createdate="
				+ createdate + ", loanRate=" + loanRate + ", loanNumber=" + loanNumber + ", typeid=" + typeid
				+ ", loanStatus=" + loanStatus + ", countMoney=" + countMoney + ", loanTime=" + loanTime + ", userid="
				+ userid + ", ordernumber=" + ordernumber + "]";
	}
}