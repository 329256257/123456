package com.icss.yrbao.bean;

import java.math.BigDecimal;
import java.util.Date;

public class Inverst {
    private Integer investid;

    private Integer userid;

    private Integer loanId;

    private BigDecimal investMoney;

    private Date createdate;

    private String ordernumber;
    
    private String username;

    public Integer getInvestid() {
        return investid;
    }

    public void setInvestid(Integer investid) {
        this.investid = investid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public BigDecimal getInvestMoney() {
        return investMoney;
    }

    public void setInvestMoney(BigDecimal investMoney) {
        this.investMoney = investMoney;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber == null ? null : ordernumber.trim();
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "Inverst [investid=" + investid + ", userid=" + userid + ", loanId=" + loanId + ", investMoney="
				+ investMoney + ", createdate=" + createdate + ", ordernumber=" + ordernumber + ", username=" + username
				+ "]";
	}
}