package com.icss.yrbao.bean;

import java.math.BigDecimal;
import java.util.Date;

public class User {
    private Integer userid;

    private String username;

    private String password;

    private String phone;

    private String email;

    private String usernumber;

    private String realname;

    private BigDecimal balance;

    private Integer cardid;

    private int status;
    
    private int usertype;
    
    private Date createdate;
    
    private Date logintime;



	@Override
	public String toString() {
		return "User [userid=" + userid + ", username=" + username + ", password=" + password + ", phone=" + phone
				+ ", email=" + email + ", usernumber=" + usernumber + ", realname=" + realname + ", balance=" + balance
				+ ", cardid=" + cardid + ", status=" + status + ", usertype=" + usertype + ", createdate=" + createdate
				+ ", logintime=" + logintime + "]";
	}



	public Integer getUserid() {
		return userid;
	}



	public void setUserid(Integer userid) {
		this.userid = userid;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getUsernumber() {
		return usernumber;
	}



	public void setUsernumber(String usernumber) {
		this.usernumber = usernumber;
	}



	public String getRealname() {
		return realname;
	}



	public void setRealname(String realname) {
		this.realname = realname;
	}



	public BigDecimal getBalance() {
		return balance;
	}



	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}



	public Integer getCardid() {
		return cardid;
	}



	public void setCardid(Integer cardid) {
		this.cardid = cardid;
	}



	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	public int getUsertype() {
		return usertype;
	}



	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}



	public Date getCreatedate() {
		return createdate;
	}



	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}



	public Date getLogintime() {
		return logintime;
	}



	public void setLogintime(Date logintime) {
		this.logintime = logintime;
	}
}