package com.icss.yrbao.serviceimpl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icss.yrbao.bean.Inverst;
import com.icss.yrbao.bean.Loan;
import com.icss.yrbao.dao.LoanMapper;
import com.icss.yrbao.service.ILoanServicer;

@Service("loanservicer")
public class LoanServiceImpl implements ILoanServicer {
	@Autowired
	private LoanMapper loanmapper;

	public LoanMapper getLoanmapper() {
		return loanmapper;
	}

	public void setLoanmapper(LoanMapper loanmapper) {
		this.loanmapper = loanmapper;
	}

	public ArrayList<Loan> findAllLoan() {
		ArrayList<Loan> loans=loanmapper.findAllLoan();
		return loans;
	}

	public Loan findLoanByid(Integer loanid) {
		Loan ln=loanmapper.findLoanByid(loanid);
		return ln;
	}



}
