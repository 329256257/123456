package com.icss.yrbao.serviceimpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.icss.yrbao.bean.Inverst;
import com.icss.yrbao.bean.Transaction;
import com.icss.yrbao.dao.InverstMapper;
import com.icss.yrbao.dao.LoanMapper;
import com.icss.yrbao.dao.TransactionMapper;
import com.icss.yrbao.dao.UserMapper;
import com.icss.yrbao.service.IInverstServicer;

@Service("inverstServicer")
@Transactional
public class InverstServicerImpl implements IInverstServicer {

	@Autowired
	private InverstMapper inverstmapper;
	@Autowired
	private UserMapper usermapper;
	@Autowired
	private TransactionMapper transactionMapper;
	@Autowired
	private LoanMapper loanmapper;
	public LoanMapper getLoanmapper() {
		return loanmapper;
	}

	public void setLoanmapper(LoanMapper loanmapper) {
		this.loanmapper = loanmapper;
	}
	
	public TransactionMapper getTransactionMapper() {
		return transactionMapper;
	}

	public void setTransactionMapper(TransactionMapper transactionMapper) {
		this.transactionMapper = transactionMapper;
	}
	public InverstMapper getInverstmapper() {
		return inverstmapper;
	}

	public void setInverstmapper(InverstMapper inverstmapper) {
		this.inverstmapper = inverstmapper;
	}

	public UserMapper getUsermapper() {
		return usermapper;
	}

	public void setUsermapper(UserMapper usermapper) {
		this.usermapper = usermapper;
	}

	public ArrayList<Inverst> findInverstByid(Integer loanid) {
		ArrayList<Inverst> inversts = inverstmapper.findInverstByid(loanid);
		return inversts;
	}

public boolean toInverat(Integer userid, Integer loanid, Integer number) {
	boolean result=false;
	String num=getOrderNum();
	result=inverstmapper.addInverat(userid,loanid,number,num);
	result=usermapper.updateBalance(userid,number);
	result=loanmapper.updateCountMoney(loanid,number);
	result=transactionMapper.insertTransaction(userid,number,num);
	return result;
}
public String getOrderNum(){
	Date date = new Date();  //当前系统时间
	SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");  //创建格式转换的对象
	String str = df.format(date);  //格式转换
	Random random = new Random();  //创建随机数对象
	int rannum = (int)(random.nextDouble()*9000)+10000;  //生成5位随机数
	return str+rannum;
}


}
