package com.icss.yrbao.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icss.yrbao.bean.User;
import com.icss.yrbao.dao.UserMapper;
import com.icss.yrbao.service.IUserServicer;
@Service("userservice")
public class UserServiceImpl implements IUserServicer{
	@Autowired
	private UserMapper usermapper;

	public UserMapper getUsermapper() {
		return usermapper;
	}

	public void setUsermapper(UserMapper usermapper) {
		this.usermapper = usermapper;
	}

    //ע��
	public User regist(User user) {
		User u=usermapper.selectUser(user);
		
		if(u==null) {
			 usermapper.insertUser(user);
			 u=usermapper.selectUser(user);
		}
		return u;
	}
    //����
	public User login(String username, String password) {
		
		User u=usermapper.loginSelect(username,password);
		usermapper.updateLogintime(username,password);
		return u;
	}
     //����userid��ѯ
	public User selectByid(Integer userid) {
		User u=usermapper.selectByid(userid);
		return u;
	}
	

}
