package com.icss.yrbao.service;

import java.util.ArrayList;

import com.icss.yrbao.bean.Inverst;

public interface IInverstServicer {

	ArrayList<Inverst> findInverstByid(Integer loanid);

	boolean toInverat(Integer userid, Integer loanid, Integer number);

}
