package com.icss.yrbao.service;

import com.icss.yrbao.bean.User;

public interface IUserServicer {

	User regist(User user);

	User login(String username, String password);

	User selectByid(Integer userid);

}
