package com.icss.yrbao.service;

import java.util.ArrayList;

import com.icss.yrbao.bean.Loan;

public interface ILoanServicer {

	ArrayList<Loan> findAllLoan();

	Loan findLoanByid(Integer loanid);


}
